FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

# Base prgramms
RUN apt-get update  && apt-get dist-upgrade -y  \
    && apt-get install -y \
    make \
    git \
    curl

# Needed Latex packages
RUN apt-get install -y \
    texlive-base \
    texlive-fonts-recommended \
    texlive-latex-recommended \
    texlive-fonts-extra \
    texlive-latex-extra \
    texlive-lang-japanese \
    texlive-science
